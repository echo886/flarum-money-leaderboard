# Money Leaderboard 社区排行榜 积分 资金
 ### A Flarum extension. Add money leaderboard to the forum. fork by Ziiven/flarum-money leaderboard

A [Flarum](http://flarum.org) extension. Add money leaderboard to the forum.  
社区排行榜 积分 资金

### Installation

Install with composer:

```sh
composer require wanecho/flarum-money-leaderboard
```

### Updating

```sh
composer update wanecho/flarum-money-leaderboard
php flarum migrate
php flarum cache:clear
```

### Links
